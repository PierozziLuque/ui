package ui;

import java.net.MalformedURLException;
import java.util.List;
import market.Market;
import market.MarketFinder;
public class SetupMarkets {
	private MarketFinder marketFinder;
	private String _path;
	private String _package;
	
	public SetupMarkets(String _path, String _package)
	{
		marketFinder = new MarketFinder();
		this._path = _path;
		this._package = _package;
	}
	
	public void initialize()
	{
		try {
			marketFinder.initialize(_path, _package);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}
	public List<String> getMarketNames()
	{
		return marketFinder.getNames();
	}
	
	public Market getMarket(String name)
	{
		return marketFinder.getMarket(name);
	}

}
