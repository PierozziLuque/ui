package ui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import bot.Bot;
import bot.BotSetupDTO;
import market.Market;
import rules.AssetRule;
import ui.BotConfigView;
import wallet.Wallet;

public class BotCreator  implements PropertyChangeListener{
	private Bot bot;
	private BotConfigView view;
	private SetupMarkets marketSetup;
	private BotSetupDTO botSetup;
	
	public BotCreator(BotSetupDTO botSetup, SetupMarkets marketSetup)
	{
		this.marketSetup = marketSetup;
		this.botSetup = botSetup;
	}
	
	public void attach(BotConfigView view)
	{
		this.view  = view;
		this.view.addPropertyChangeListener(this);
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent change) {
		try {
			Market market = marketSetup.getMarket((String) change.getNewValue());
			if(market == null)
			{
				throw new RuntimeException();
			}
			boolean createBot = view.showConfirmation("Mercado cargado exitosamente. Desea crear al Bot?");
			if(createBot) {
				view.hide();
				updateReferencePrices(market);
				Wallet wallet = new Wallet();
				wallet.addAmount(botSetup.getAmount());
				bot = new Bot(wallet, botSetup.getRules(), botSetup.getLoopTime(), market);
				view.showMessageDialog("Bot creado exitosamente");
				bot.start();				
			}
		}
		catch(Exception e)
		{
			view.showMessageDialog("Ocurrió un error inesperado, no fue posible crear al Bot");
		}
	}
	
	private void updateReferencePrices(Market market)
	{
		for(AssetRule rule: botSetup.getRules())
		{
			rule.setReferencePrice(market.getPrice(rule.getSymbol()));
		}
	}
}
