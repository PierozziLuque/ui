package ui;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Enumeration;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JLabel;

public class BotConfigView extends JFrame {
	private PropertyChangeSupport changes = new PropertyChangeSupport(this);
	private ButtonGroup btnGroup;
	private static final long serialVersionUID = 1L;
	private JFrame frame;
	private JButton acceptButton;
	private List<String> classes;
	
	public BotConfigView(List<String> classes) {
		this.classes = classes;
		this.initialize();
		addComponents();
		show();
	}
	
	public void setNames(List<String> names)
	{
		this.classes = names;
	}

	public void addComponents() {
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		int x = 10;
		int y = 10;
		JLabel title = new JLabel("Seleccione  el Mercado a utilizar:");
		title.setBounds(x, y, 300, 30);
		y+= 40;
		panel.add(title);
		btnGroup = new ButtonGroup();
		for(String nameClass : classes)
		{
			JRadioButton radioBtn = new JRadioButton(nameClass);
			radioBtn.setBounds(x,y, 150, 30);
			y+= 40;
			btnGroup.add(radioBtn);
			panel.add(radioBtn);
		}
		acceptButton = new JButton("Aceptar");
		acceptButton.addActionListener(new ActionListener() {

	            @Override
			public void actionPerformed(ActionEvent e) {
	            	for (Enumeration<AbstractButton> buttons = btnGroup.getElements(); buttons.hasMoreElements();) 
	            	{ 
	            		AbstractButton button = buttons.nextElement(); 
	            		if (button.isSelected()) 
	            		{ 
	            			changes.firePropertyChange("market", null, button.getText());
	            		} 
	            	}
	            }

	    });
		acceptButton.setBounds(x, y, 100, 30);
		panel.add(acceptButton);
		JButton cancelButton = new JButton("Cancelar");
		cancelButton.addActionListener(new ActionListener() {

	            @Override
			public void actionPerformed(ActionEvent e) {
	            	exit();
	            }

	    });
		cancelButton.setBounds(x+110, y, 100, 30);
		panel.add(cancelButton);
		y+=40;
		frame.setBounds(100, 100, 350, y+40);
		panel.setBounds(0, 0, 350, y+40);
	}

	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Configuración del Bot");
		frame.setVisible(false);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
	}

	public void show() {
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				exit();
			}
		});
		this.frame.setVisible(true);
	}
	
	public boolean showConfirmation(String message)
	{
		int confirm = JOptionPane.showOptionDialog(null, message,
				"Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
		return confirm == 0 ? true: false;
	};
	
	public void showMessageDialog(String message)
	{
		JOptionPane.showMessageDialog(null, message);
	}
	
	public void exit()
	{
		boolean confirm = showConfirmation("¿Estás seguro de que quieres salir?");
	    if(confirm)
	    	System.exit(0);
	}
	
	public void hide() {
		frame.setVisible(false);
		frame.dispose();
	}
	
	public void addPropertyChangeListener(PropertyChangeListener l) {
		changes.addPropertyChangeListener(l);
	}

}
